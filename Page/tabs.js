// TODO: Everything.


var valsList = [];

// TODO: Store in .json without carving it into a js file
var testa = {
    desc: "Testing item A",
    id: "TestA",
    img: "test2.png",
    tags: ["Test1", "Test3"]
}

var testb = {
    desc: "Testing item B",
    id: "TestB",
    img: "test1.png",
    tags: ["Test2", "Test5", "Test3"]
}

var allVals = [testa, testb];

function openCat(evt, name) {
    var i, tabcontent, tablinks;

  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  evt.currentTarget.className += " active";
  populate(name);

}
function inc(evt, f, n) {
    console.log(f);
    var field = document.getElementById(f);
    var val = parseInt(field.value);
    console.log(field);
    console.log(val);
    val = val + n;
    if (val < field.max && val > field.min){
        field.value = val;
    }
}
// populates the contents of a tab upon opening
function populate(tag) {
    var out = "";
    document.getElementById("tabdiv").innerHTML = "";
    //console.log(tag);
    for (i = 0; i < allVals.length; i++) {
        for (ii = 0; ii < allVals[i].tags.length; ii++){
                //console.log(allVals[i].tags[ii]);
                if (allVals[i].tags[ii] == tag){
                    var id = "time"+i;
                    out += "<p><div id = "+tag+" class=\"tabcontent\">";
                    out += "<img src = "+allVals[i].img+"></p>";
                    out += allVals[i].id;
                    out += "<button class=\"minusb\" onClick=\"inc(event, '"+id+"', -1)\">-</button>";
                    out += "<input type=\"number\" id='"+id+"' value=5 min=0 max=20>";
                    out += "<button class=\"plusb\" onClick=\"inc(event, '"+id+"', 1)\">+</button>";
                    out += "<button class=\"add\" onClick=\"add(event, "+i+", document.getElementById('"+id+"').value)\">ADD</button>";
                    out += "</div>";
                    
                    
                }
        }
    }
    //console.log(out);
    document.getElementById("tabdiv").innerHTML = out;
    
    //console.log(document.getElementById("tabdiv").innerHTML);

} 
// Adds an item to the playlist
function add(evt, id, t) {
    console.log(t);
    var added = {
        id: id,
        time: parseInt(t)
    }
    valsList.push(added);
    console.log(valsList);
    popPlaylist();
}
// Populates the playlist.
function popPlaylist(){
    var out = "";
    var total = 0;
    for (i = 0; i < valsList.length; i++){
        var tag = "list"+valsList[i].id;
        var id = valsList[i].id;
        total += valsList[i].time;
        console.log(valsList[i]);
        out += "<div id="+tag+" class=listcontent>";
        out += "<table><tr><td style = 'width:30%'><div class='icon'><img src="+allVals[id].img+"></td></div>";
        out += "<td><span class='desc'>";
        
        out += "<p>"+allVals[id].desc+"</p>";
        out += "<p>"+valsList[i].time+" minutes</p></td><td>";
        out += "<button class = rmb onClick=rem("+i+")>X</button>";
        out += "</td></tr></table></div>";
    }
    document.getElementById("list").innerHTML = out;
    document.getElementById("tally").innerHTML = "Total: "+total+" minutes";
    console.log(document.getElementById("list").innerHTML);
}
// Removes an item from the list
function rem(i) {
    valsList.splice(i, 1);
    popPlaylist();

}