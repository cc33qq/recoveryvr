from django.db import models

# Create your models here.

class Detailof(models.Model):

    cateid = models.TextField(blank=True, null=True)
    subcateid = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)  # This field type is a guess.
    picdir = models.ImageField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'Detailof'


