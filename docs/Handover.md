Currently, the website is in a state that should be able to be deployed to AWS Elastic Beanstalk as a Django project as-is,
and mostly represents the expected user experience.

It still requires a large amount of work to be feature-complete.

Most of the user interface runs off hand-coded javascript. Some parts may be better off migrated to the Bootstrap frontend.

HTTP communication is still essentially entirely unimplemented. The site must be able to connect and communicate with remote VR devices.
In addition, the database currently used only stores public static file data, and is not ready to accept and securely store incoming information.
This will also need to be implemented, preferrably integrating with the hosting provider's own encrypted database services.

There are credentials for a development account configured, which should hopefully be provided elsewhere.
If all else fails, the owner of the root AWS account should be able to terminate and recreate it with new details.

It is recommended to wipe the Django application's own superuser database to a clean slate for safety, and create your own accounts and credentials.

HTTPS should also be configured, which is a nontrivial process which requires certificates that should preferrably be signed by a primary stakeholder,
rather than a temporary assistant.

Consult the manuals for Django, AWS Elastic Beanstalk, and Amazon RDB for further information on these subjects.