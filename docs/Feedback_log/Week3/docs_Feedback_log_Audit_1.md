# Feedback log
## MVP in project - 08/08/2019
Feedback: The MVP delivery date is before the UI. This does not seem credible. You may have a different view of what counts as a MVP. Discuss further with your client.
Too early to tell but from our two meetings I have found they are listening to the outputs I am prioritizing and working towards them. The key now will be to pick up the goals and run with them without my prompting

Time: 8/08/2019

Sourse: Richard, client

Action: We discussed MVP with our client in 16th Aug. We have basic understanding of what our client wants. Currently, our MVP is website UI. We will bring the client the best javascript playlist template we can find next week. We are going to use HTML for website's interface, and javascript for functions.

## Documents in project - 08/08/2019
Feedback: Too much jargon in all the documents. Do not expect your client, the users, nor the tutor to understand it. Remove from all reporting.

Time: 8/08/2019

Sourse: Richard

Action: We replace jargon in all the documents to make our document easier to understand.

## Reflection in project - 08/08/2019

Feedback: Little evidence of reflection. Communicate better if you have reflected and acted.

Time: 8/08/2019

Sourse: Richard

Action: All members are assigned to edit documents. Feedback, risk, decisions and meeting mins can be found here. We also have two meetings every week.





