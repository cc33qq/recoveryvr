# Decision log

## Data restrictions - 29/08/2019

Reasoning: Upon further research into industry standards, in hindsight cloud services are generally HIPAA-certified
and using in-house servers with our expertise is an insane idea. Amazon AWS is currently the preferred platform for initial deployment.

Outcome: Full agreement.

## Back-end Framework selection - 26/08/2019

Reasoning: Web development in bare code is not recommended for a team of novices.
Have to select an appropriate framework to assist in creating a functional, secure, and responsive site.

Outcome: Django framework pushed.

## Switching to Gitlab - 19/08/2019

Reasoning: Github's paywalls prevent private repositories with more than 3 contributors.

Outcome: All agreed to abandon Github.

## Postponing testing with real data - 15/08/2019

Reasoning: Medical data remains a sensitive issue, however, the remote datase is not an immediate concern for the project.
As such, it is not necessary to deal with such data immediately, and the system may be limited to placeholder data
until it is secure and feature-complete.

Outcome: All agreed.

## Decoupling personal data from stored records - 09/08/2019

Reasoning: Proposing systems that will handle personal medical data is a sensitive topic.
Thus, avoiding handling such data in the first place means a circumvented risk.

Outcome: Members agreed, but overruled by client.

## Using Agile as development method - 01/08/2019
Proposed member: Xin Kan

Reasoning: Our project is a startup project and our members have little experience with our jobs. So our team can not give a concrete schedule of the work which means our team can not use the waterfall design method.

Outcome: All members agreed
